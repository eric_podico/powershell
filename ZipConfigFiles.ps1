#############################################################################################
$HStringFunctionZipFileType = @'
 
Zip-FileType compresses all the files of a specified type in a specified folder
that are older than a specified number of days.  It leaves the resulting zip file in the
folder specified and then deletes the original files that have been compressed.

Usage:  Zip-FileType $LastDelivery "C:\Attolight\Service\ConfigArchive\" @("*.json","*.ini","*.xml", "*.xml") 

        All @("*.json","*.ini","*.xml", "*.xml") files from the LastDelivery folder 
        are going to be copied and zipped in ConfigArchive folder
        
        $LastDelivery folder path is found in this inin file : c:\Attolight\tool.inf

        Produces a zip file called according to the date and the archive type"
        i.e.: Files_archived_on__2019-09-02_172834

Autor:
    epo

History : 
    2019-09-02 first draft

'@ 
############################################################################################# 

$Version = "Version 1.0"


Function Zip-FileType ([string]$FileFolder,[string]$ArchiveFileFolder,[string[]]$FileType) {

Begin {

    Write-Host "ZipConfigFiles " 
    Write-Host "  LastDelivery =  "    $LastDelivery

    If ($FileFolder -and $FileType) {
        Add-Type -A System.IO.Compression.FileSystem
        If (Test-Path $FileFolder) {
            $ZipDate = (Get-Date)
            # Get the list of files to be archived
            $FilesToArchive = Get-ChildItem -Path $FileFolder\* -Include $FileType 
            
            Write-Host "  FileType =  "    $FileType
            Write-Host "  FilesToArchive =  "    $FilesToArchive
            Write-Host " "$Version

            If (-not $FilesToArchive){Write-Host "There were no files to archive"}
        }
        Else {Write-Host "  N\ot a valid Folder name"}
    }
}

Process {
    If ($FilesToArchive) { # There are files in the folder that need archiving
        # Create the name of the archive
        $ArchiveName = "Files_archived_on_" +
                       "$($ZipDate.Year)-" +
                       "$("{0:00}" -f $ZipDate.Month)-" +
                       "$("{0:00}" -f $ZipDate.Day)_" +
                       "$("{0:00}" -f $ZipDate.Hour)"+
                       "$("{0:00}" -f $ZipDate.Minute)"+
                       "$("{0:00}" -f $ZipDate.Second)"

        If (Test-Path "$ArchiveFileFolder\$ArchiveName"){
            Write-Host "  The temporary archive folder already exists"
        }
        Else {
            # Create the temporary folder needed for the archiving
            New-Item -Path $ArchiveFileFolder -Name $ArchiveName -ItemType directory
            # Copy the files to be archived to the temporary folder
            $FilesToArchive | ForEach-Object {
                Copy-Item -Path $_ -Destination "$ArchiveFileFolder\$ArchiveName"   

                Write-Host "  Item =  "    $_
                
            }
            If (Test-Path "$ArchiveFileFolder\$ArchiveName.zip"){
                Write-Host "Archive file already exists"
            }
            Else {
                # Compress the files
                [IO.Compression.ZipFile]::CreateFromDirectory("$ArchiveFileFolder\$ArchiveName",
                                                              "$ArchiveFileFolder\$ArchiveName.zip")
                # Remove the temporary folder
                Remove-Item -Path "$ArchiveFileFolder\$ArchiveName" -Recurse -Force
            }
        }
    }
}
End {
    If (-not $FileFolder) {
        $HStringFunctionZipFileType
    }
}
}

function Get-IniFile 
{  
    param(  
        [parameter(Mandatory = $true)] [string] $filePath  
    )  

    $anonymous = "NoSection"

    $ini = @{}  
    switch -regex -file $filePath  
    {  
        "^\[(.+)\]$" # Section  
        {  
            $section = $matches[1]  
            $ini[$section] = @{}  
            $CommentCount = 0  
        }  

        "^(;.*)$" # Comment  
        {  
            if (!($section))  
            {  
                $section = $anonymous  
                $ini[$section] = @{}  
            }  
            $value = $matches[1]  
            $CommentCount = $CommentCount + 1  
            $name = "Comment" + $CommentCount  
            $ini[$section][$name] = $value  
        }   

        "(.+?)\s*=\s*(.*)" # Key  
        {  
            if (!($section))  
            {  
                $section = $anonymous  
                $ini[$section] = @{}  
            }  
            $name,$value = $matches[1..2]  
            $ini[$section][$name] = $value  
        }  
    }  

    return $ini  
}  

$iniFile = Get-IniFile c:\Attolight\tool.inf
$LastDelivery = $iniFile.Settings.LastDelivery


# Put in your own parameters and uncomment the below to test


Zip-FileType $LastDelivery "C:\Attolight\Service\ConfigArchive\" @("*.json","*.ini","*.xml", "*.xml")
